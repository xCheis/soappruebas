package jaxws.service;

import javax.jws.WebService;

@WebService(endpointInterface="jaxws.service.MensajeService")
public class MensajeServiceImpl implements MensajeService {

	@Override
	public String Mensaje(String mensaje) {
		if(mensaje!=null) {
			return ("Tu mensaje es: " + mensaje);
		} 
			return null;
	}
	
	

}
