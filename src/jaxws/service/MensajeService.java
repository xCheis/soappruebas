package jaxws.service;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface MensajeService {
	
	@WebMethod
	public String Mensaje (String mensaje);

}
